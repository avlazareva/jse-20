package ru.t1.lazareva.tm.exception.entity;

import ru.t1.lazareva.tm.exception.AbstractException;

public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
    }

    public AbstractEntityException(final String message) {
        super(message);
    }

    public AbstractEntityException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}