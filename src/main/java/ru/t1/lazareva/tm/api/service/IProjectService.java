package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.api.repository.IUserOwnedRepository;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    List<Project> findAll(String userId, Sort sort);

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}